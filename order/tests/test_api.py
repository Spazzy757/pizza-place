# -*- coding: utf-8 -*-
from order.models import Order, Ingredient, Pizza
from .test_helpers import populate_ingredients
from django.test import TestCase
from django.urls import reverse
import json


class CreateOrderTestCase(TestCase):

    def setUp(self):
        self.url = reverse('order-list')
        self.client.post(self.url, data={
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
        })

    def test_create_order_with_post(self):
        self.assertEqual(Order.objects.count(), 1,
                         msg="Order Not Created")


class UpdateOrderTestCase(TestCase):

    def setUp(self):
        self.url = reverse('order-list')
        self.order = Order.objects.create(**{
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
        })

    def test_update_order_with_patch(self):
        response = self.client.patch("{}{}/".format(self.url, self.order.id),
                                     data=json.dumps({
                                         'customer_name': 'Tyrion Lannister'
                                     }),
                                     content_type="application/json")
        self.assertEqual(response.json().get('customer_name'),
                         'Tyrion Lannister',
                         msg="Order Not Updated")

    def test_update_fails_for_invalid_order(self):
        order = Order.objects.create(**{
            'customer_name': 'Jamie Lannister',
            'contact_number': '07260076820',
            'status': Order.DELIVERED
        })
        response = self.client.patch("{}{}/".format(self.url, order.id),
                                     data=json.dumps({
                                         'customer_name': 'Tyrion Lannister'
                                     }),
                                     content_type="application/json")
        self.assertEqual(response.status_code, 400, msg='Validation Failed')


class DeleteOrderTestCase(TestCase):

    def setUp(self):
        self.url = reverse('order-list')
        self.order = Order.objects.create(**{
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
        })

    def test_selete_order_with_delete(self):
        response = self.client.delete("{}{}/".format(self.url, self.order.id))
        self.assertEqual(response.status_code, 204,
                         msg="Order Not Deleted")


class ListAndRetrieveOrderTestCase(TestCase):

    def setUp(self):
        self.url = reverse('order-list')
        self.order = Order.objects.create(**{
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
        })
        Order.objects.create(**{
            'customer_name': 'Tyrion Lannister',
            'contact_number': '07260076834',
        })
        self.response = self.client.get(self.url)

    def test_create_order_with_post(self):
        self.assertEqual(self.response.json().get('count'), 2,
                         msg="Order Not Created")

    def test_retrieve_order_by_id(self):
        response = self.client.get("{}{}/".format(self.url, self.order.id))
        self.assertEqual(response.json().get('id'), self.order.id,
                         msg="Retrieving order failed")


class AddPizzaTestCase(TestCase):

    def setUp(self):
        self.url = reverse('pizza-list')
        populate_ingredients()
        self.order = Order.objects.create(**{
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
        })
        self.cheese = Ingredient.objects.get(name='mozzarella cheese')
        self.pepperoni = Ingredient.objects.get(name='pepperoni')

    def test_add_pizza_to_order(self):
        data = {
            'size': Pizza.MEDIUM,
            'ingredients': [self.pepperoni.id, self.cheese.id],
            'order': self.order.id
        }
        self.client.post(self.url, data=data)
        self.assertEqual(Pizza.objects.count(), 1, msg='Failed Adding Pizza')


class InvalidOrderUpdateTestCase(TestCase):

    def setUp(self):
        self.url = reverse('pizza-list')
        populate_ingredients()
        self.order = Order.objects.create(**{
            'customer_name': 'John Snow',
            'contact_number': '07260076820',
            'status': Order.DELIVERED
        })
        self.cheese = Ingredient.objects.get(name='mozzarella cheese')
        self.pepperoni = Ingredient.objects.get(name='pepperoni')

    def test_add_pizza_to_order_fails(self):
        data = {
            'size': Pizza.MEDIUM,
            'ingredients': [self.pepperoni.id, self.cheese.id],
            'order': self.order.id
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400, msg='Validation Failed')
