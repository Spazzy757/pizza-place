from order.models import Ingredient


def populate_ingredients():
    ingredients = ['mozzarella cheese', 'pepperoni', 'spinach', 'feta', 'ham'
                   'chili', 'garlic', 'pepper']
    for ingredient in ingredients:
        Ingredient.objects.create(**{
            'name': ingredient,
            'code': ingredient
        })
