from order.serializers import OrderSerializer, PizzaSerializer, NOT_CHANGEABLE
from rest_framework.exceptions import ValidationError
from rest_framework.viewsets import ModelViewSet
from order.models import Order, Pizza
# Create your views here.


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def partial_update(self, request, *args, **kwargs):
        """
        Check that the order is not in Delivered Status or Out for Delivery
        (Cant Be Done in serializer as order is not passed through in a partial
        update and therefore would not validate against the order)
        """
        if Order.objects.get(id=kwargs.get('pk')).status in NOT_CHANGEABLE:
            raise ValidationError("Order is not changeable at this point")
        instance = super(ModelViewSet, self)
        return instance.partial_update(request, *args, **kwargs)


class PizzaViewSet(ModelViewSet):
    queryset = Pizza.objects.all()
    serializer_class = PizzaSerializer
