from order.views import OrderViewSet, PizzaViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(
    prefix='order',
    viewset=OrderViewSet,
    base_name='order'
)

router.register(
    prefix='pizza',
    viewset=PizzaViewSet,
    base_name='pizza'
)

urlpatterns = router.urls
