from django.db import models


# Create your models here.
class Ingredient(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.name)


class Order(models.Model):
    CAPTURED = 'c'
    IN_PROGRESS = 'p'
    OUT_FOR_DELIVERY = 'o'
    DELIVERED = 'd'
    ORDER_STATUSES = (
        (CAPTURED, 'Order Placed'),
        (IN_PROGRESS, 'In Progress'),
        (OUT_FOR_DELIVERY, 'Out For Delivery'),
        (DELIVERED, 'Delivered')
    )
    customer_name = models.CharField(max_length=255)
    contact_number = models.CharField(max_length=15)
    status = models.CharField(max_length=10,
                              default=CAPTURED,
                              choices=ORDER_STATUSES)
    # Optional for promotions
    email = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return "{}: {}".format(self.id, self.customer_name)


class Pizza(models.Model):
    SMALL = 's'
    MEDIUM = 'm'
    LARGE = 'l'
    SIZE_CHOICES = (
        (SMALL, 'Small'),
        (MEDIUM, 'Medium'),
        (LARGE, 'Large')
    )
    size = models.CharField(max_length=10,
                            default=SMALL,
                            choices=SIZE_CHOICES)
    ingredients = models.ManyToManyField(Ingredient,
                                         related_name='ingredients')
    order = models.ForeignKey(Order,
                              related_name='pizzas',
                              on_delete=models.CASCADE)
