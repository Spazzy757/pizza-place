# Generated by Django 2.1 on 2018-12-24 13:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('code', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_name', models.CharField(max_length=255)),
                ('contact_number', models.CharField(max_length=15)),
                ('status', models.CharField(choices=[('c', 'Order Placed'), ('p', 'In Progress'), ('o', 'Out For Delivery'), ('d', 'Delivered')], default='c', max_length=10)),
                ('email', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pizza',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('size', models.CharField(choices=[('s', 'Small'), ('m', 'Medium'), ('l', 'Large')], default='s', max_length=10)),
                ('ingredients', models.ManyToManyField(related_name='ingredients', to='order.Ingredient')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pizzas', to='order.Order')),
            ],
        ),
    ]
