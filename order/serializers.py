from rest_framework.serializers import ModelSerializer, ValidationError
from order.models import Order, Pizza

NOT_CHANGEABLE = [Order.DELIVERED, Order.OUT_FOR_DELIVERY]


class PizzaSerializer(ModelSerializer):
    class Meta:
        model = Pizza
        fields = ('id', 'size', 'ingredients', 'order')

    @staticmethod
    def validate_order(value):
        """
        Check that the order is not in Delivered Status or Out for Delivery
        """
        if value.status in NOT_CHANGEABLE:
            raise ValidationError("Order is not changeable at this point")
        return value


class OrderSerializer(ModelSerializer):
    pizzas = PizzaSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'customer_name', 'contact_number', 'status', 'email',
                  'pizzas')
