# pizza-place

**Notice**: This project has been setup using [docker](https://www.docker.com/) and 
[docker-compose](https://docs.docker.com/compose/)

To start project for the first time:
```bash
# Some times it takes a bit for the db to start up for the firts time 
# so if the command fails run it again

docker-compose up

# To Run Migrations

docker-compose run --rm web python manage.py migrate

# To create a super user (run command and follow the prompts)

docker-compose run --rm web python manage.py createsuperuser

# Setup Static files for admin interface (This will not work on a production instance
# you will need to hots your files and serve them)

docker-compose run --rm web python manage.py collectstatic

``` 
> Once up and running visit [http://localhost:8000/api/](http://localhost:8000/api/) 
for the api endpoints and [http://localhost:8000/admin/](http://localhost:8000/admin/)
for the admin interface

## Running the tests

```bash
# To run unit tests

docker-compose run --rm web coverage run manage.py test

# To show test coverage

docker-compose run --rm web coverage report
```
> tests can be found in /order/tests/test_api.py