import os


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', 'postgres'),
        'USER': os.environ.get('DATABASE_USER', 'postgres'),
        # TODO: Make an env
        'HOST': os.environ.get('DATABASE_HOST', 'db'),
        'PORT': os.environ.get('POSTGRES_DB_PORT', '5432'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
    }
}

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20
}

STATIC_ROOT = "/code/static/"
